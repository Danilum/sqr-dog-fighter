package com.example.dog.fighter.service;

import com.example.dog.fighter.DogDTO;
import com.example.dog.fighter.entity.Dog;
import com.example.dog.fighter.repository.DogRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.*;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@SpringBootTest
public class DogServiceTest {

    @Autowired
    private DogService dogService;

    @MockBean
    private DogRepository dogRepository;

    @Test
    @DisplayName("No invokations test")
    void verificationTest0(){
        DogService dogyyService = mock(DogService.class);
        verify(dogyyService, times(0)).getListOfAllDogs();
        verify(dogyyService, times(0)).increaseRating(anyInt());
        verify(dogyyService, times(0)).createDog(any());
        verify(dogyyService, times(0)).existsById(anyInt());
        verify(dogyyService, times(0)).existsByImage(anyString());
    }

    @Test
    @DisplayName("Get list of no dogs test")
    void getListOfAllDogsTest(){
        when(dogRepository.findAll()).thenReturn(new ArrayList<>());
        Assertions.assertEquals(0,dogService.getListOfAllDogs().size());
    }

    @Test
    @DisplayName("Get list of all dogs test")
    void getListOfAllDogsTest2(){
        Dog dog1 = new Dog(1, "Igor", "http://vk.com/igorlmk", 2);
        Dog dog2 = new Dog(2, "Harvard", "http://vk.com/igorsam9", 3);

        DogDTO dog1DTO = new DogDTO();
        BeanUtils.copyProperties(dog1,dog1DTO);
        DogDTO dog2DTO = new DogDTO();
        BeanUtils.copyProperties(dog2,dog2DTO);

        when(dogRepository.findAll()).thenReturn(new ArrayList<>(Arrays.asList(dog1,dog2)));
        Assertions.assertEquals(2,dogService.getListOfAllDogs().size());
        Assertions.assertEquals(true,dogService.getListOfAllDogs().contains(dog1DTO));
        Assertions.assertEquals(true,dogService.getListOfAllDogs().contains(dog2DTO));
    }

    @Test
    @DisplayName("Incresae rating of non-existing dog test")
    void increaseRatingTest(){
        Optional<Dog> optionalEmptyDog = Optional.empty();
        when(dogRepository.findById(anyInt())).thenReturn(optionalEmptyDog);
        Assertions.assertEquals(false,dogService.increaseRating(5));
    }

    @Test
    @DisplayName("Incresae rating of a dog test")
    void increaseRatingTest2(){
        Dog dog1 = new Dog(1, "Igor", "http://vk.com/igorlmk", 2);
        Optional<Dog> optionalDog = Optional.of(dog1);
        when(dogRepository.findById(1)).thenReturn(optionalDog);
        dog1.setRating(dog1.getRating()+1);

        Assertions.assertEquals(true,dogService.increaseRating(1));
        Assertions.assertEquals(false,dogService.increaseRating(5));
        verify(dogRepository).save(dog1);
    }

    @Test
    @DisplayName("Existance of a dog by image test")
    void existsByImageTest(){
        Dog dog1 = new Dog(1, "Igor", "image", 2);
        Optional<Dog> optionalDog = Optional.of(dog1);
        when(dogRepository.findDogByImage("image")).thenReturn(optionalDog);
        Assertions.assertEquals(true,dogService.existsByImage("image"));
        Assertions.assertEquals(false,dogService.existsByImage("none"));
    }

    @Test
    @DisplayName("NO Existance of a dog by image test")
    void existsByImageTest2(){
        Optional<Dog> optionalEmptyDog = Optional.empty();
        when(dogRepository.findDogByImage("image")).thenReturn(optionalEmptyDog);
        Assertions.assertEquals(false,dogService.existsByImage("image"));
        Assertions.assertEquals(false,dogService.existsByImage("none"));
    }

    @Test
    @DisplayName("Existance of a dog by id test")
    void existsByIdTest(){
        Dog dog1 = new Dog(1, "Igor", "image", 2);
        Optional<Dog> optionalDog = Optional.of(dog1);
        when(dogRepository.findById(1)).thenReturn(optionalDog);
        Assertions.assertEquals(true,dogService.existsById(1));
        Assertions.assertEquals(false,dogService.existsById(2));
    }

    @Test
    @DisplayName("NO Existance of a dog by image test")
    void existsByIdTest2(){
        Optional<Dog> optionalEmptyDog = Optional.empty();
        when(dogRepository.findById(anyInt())).thenReturn(optionalEmptyDog);
        Assertions.assertEquals(false,dogService.existsById(1));
        Assertions.assertEquals(false,dogService.existsById(2000000));
    }

    @Test
    @DisplayName("Create dog test")
    void createDogTest(){
        DogDTO dog1Initial = new DogDTO(1, "Igor", "image", 2);
        Dog dog1 = new Dog();
        BeanUtils.copyProperties(dog1Initial,dog1);
        dog1.setRating(0);
        when(dogRepository.saveAndFlush(dog1)).thenReturn(dog1);
        DogDTO dog1After = new DogDTO();
        BeanUtils.copyProperties(dog1,dog1After);
        Assertions.assertEquals(dog1After,dogService.createDog(dog1Initial));
    }

    private String[] getNullPropertyNames(Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        Set emptyNames = new HashSet();

        for(java.beans.PropertyDescriptor descriptor : src.getPropertyDescriptors()) {

            if (src.getPropertyValue(descriptor.getName()) == null) {
                emptyNames.add(descriptor.getName());
            }
        }

        String[] result = new String[emptyNames.size()];
        return (String[]) emptyNames.toArray(result);
    }
}
