package com.example.dog.fighter.service;

import com.example.dog.fighter.DogDTO;
import com.example.dog.fighter.entity.Dog;
import com.example.dog.fighter.repository.DogRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Component
@Transactional
public class DogService {

    @Autowired
    DogRepository dogRepository;

    public boolean existsByImage(String image){
        return dogRepository.findDogByImage(image).isPresent();
    }

    public boolean existsById(Integer id){
        return dogRepository.findById(id).isPresent();
    }

    public DogDTO createDog(DogDTO detail){
        Dog entity = new Dog();
        BeanUtils.copyProperties(detail,entity);
        entity.setRating(0);
        entity = dogRepository.saveAndFlush(entity);
        BeanUtils.copyProperties(entity,detail);
        return detail;
    }

    public List<DogDTO> getListOfAllDogs(){
        List<Dog> dogs = dogRepository.findAll();
        List<DogDTO> dogDTOS = new ArrayList<>();
        for (Dog dog: dogs) {
            DogDTO dogDTO = new DogDTO();
            BeanUtils.copyProperties(dog,dogDTO);
            dogDTOS.add(dogDTO);
        }

        return dogDTOS;
    }

    public boolean increaseRating(Integer id){
        Optional<Dog> dog = dogRepository.findById(id);
        try{
            if(dog.isPresent()){
                Dog dog1 = dog.get();
                dog1.setRating(dog1.getRating()+1);
                dogRepository.save(dog1);
                return true;
            } else return false;

        }
        catch (Exception e){
            return false;
        }
    }

    private String[] getNullPropertyNames(Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        Set emptyNames = new HashSet();

        for(java.beans.PropertyDescriptor descriptor : src.getPropertyDescriptors()) {

            if (src.getPropertyValue(descriptor.getName()) == null) {
                emptyNames.add(descriptor.getName());
            }
        }

        String[] result = new String[emptyNames.size()];
        return (String[]) emptyNames.toArray(result);
    }
}
