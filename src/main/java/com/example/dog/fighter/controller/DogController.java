package com.example.dog.fighter.controller;

import com.example.dog.fighter.DogApiClient;
import com.example.dog.fighter.DogDTO;
import com.example.dog.fighter.service.DogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "/**")
public class DogController {

    @Autowired
    DogApiClient dogApiClient;

    @Autowired
    DogService dogService;

    @GetMapping("/find")
    public ResponseEntity<DogDTO> findByBreed(@RequestParam(required = true) String breedName) {

        DogDTO response = new DogDTO();
        try {
            if(dogApiClient.getImages(breedName).getStatus().equals("success")) {
                response.setImage(dogApiClient.getImages(breedName).getMessage());
                response.setBreed(breedName);
                response.setRating(0);
            }

            return ResponseEntity.ok(response);

        } catch(Exception e){
            return ResponseEntity.ok(response);
        }
    }

    @PostMapping("/addToFight")
    public ResponseEntity<Boolean> addToFight(@RequestBody DogDTO dogDTO) {

        if(!dogService.existsByImage(dogDTO.getImage()))
            throw new ResponseStatusException(HttpStatus.IM_USED, "dog is already on the fight, try new");
        else {
            dogService.createDog(dogDTO);
            return ResponseEntity.ok(true);
        }
    }

    // look at situation when randomValue1=randomValue2
    @GetMapping("/getPair")
    public ResponseEntity<List<DogDTO>> getPair() {

        List<DogDTO> dogs = dogService.getListOfAllDogs();
        List<DogDTO> pair = new ArrayList<>();

        if(dogs.size()==2){
            DogDTO dog = dogs.get(0);
            pair.add(dog);
            dog = dogs.get(1);
            pair.add(dog);
        } else if(dogs.size()>2){
            int randomValue1 = (int) (Math.random() * dogs.size());
            int randomValue2 = (int) (Math.random() * dogs.size());
            DogDTO dog = dogs.get(randomValue1);
            pair.add(dog);
            dog = dogs.get(randomValue2);
            pair.add(dog);
        }
        return ResponseEntity.ok(pair);
    }

    @GetMapping("/vote")
    public ResponseEntity<Boolean> vote(@RequestParam(required = true) Integer dogId) {
        if(dogService.existsById(dogId)){
            return ResponseEntity.ok(dogService.increaseRating(dogId));
        } else throw new ResponseStatusException(HttpStatus.NOT_FOUND, "there is no dog with this id"+ dogId);
    }

    @GetMapping("/rating")
    public ResponseEntity<List<DogDTO>> getRatingList() {
        return ResponseEntity.ok(dogService.getListOfAllDogs());
    }
}