package com.example.dog.fighter;

import lombok.Data;

import java.util.List;

@Data
public class ImagesDTO {
    String message;
    String status;

    public ImagesDTO(){}

    public ImagesDTO(String message, String status) {
        this.message = message;
        this.status = status;
    }
}
