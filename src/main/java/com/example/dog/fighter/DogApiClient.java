package com.example.dog.fighter;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "dog", url = "https://dog.ceo/api/")
public interface DogApiClient {



    //@RequestMapping(method = RequestMethod.GET, value = "breed/{breedName}/images/random")

//    @GetMapping(value = "breed/hound/images/random")
//    public ImagesDTO getImages();

    @GetMapping(value = "breed/{breedName}/images/random")
    public ImagesDTO getImages(@PathVariable String breedName);

}
